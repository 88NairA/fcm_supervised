function [  ] = myPlot( lpop )
 syms x1 x2;
 x = [x1 x2];
 ezsurfc(20*(1-exp(-0.2*sqrt(mean(x.^2))))+exp(1)-exp(mean(cos(2*pi*x))),[-32,32])
hold on;
num=numel(lpop);
for ii=1:num
    x(ii,1) = lpop(ii).Position(1,1);
    y(ii,1) = lpop(ii).Position(1,2);
    z(ii,1) = lpop(ii).Cost;
end
for i=1:num
    plot3(lpop(i).Position(1,1),lpop(i).Position(1,2),lpop(i).Cost,'p','color','black','MarkerSize',10,'MarkerFaceColor','black')
hold on;
end
for i=1:num
    plot3(lpop(i).Position(1,1),lpop(i).Position(1,2),0,'p','MarkerSize',8)
hold on;
end

end


