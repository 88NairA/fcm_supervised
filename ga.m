close all
clc; clear
global Wrows Wcols weightsNo;

%%  main data
load FCM_mainData_7Nodes.mat 
data = FCM_mainData;

load FCM_mainWeights_7Nodes.mat

[Wrows Wcols] = find(FCM_mainWeights ~=0);
weightsNo = size(Wcols,1);

%% Problem Definition
%% FCM Structure - number of Nodes 
FCMStr.NodesNo = size(data,2);
FCMStr.landa = 1;

%% Problem Definition

CostFunction=@(x,y) FCM_CostFunction(x,y);   % Cost Function

nVar=weightsNo;             % Number of Decision Variables

VarSize=[1 nVar];   % Decision Variables Matrix Size

VarMin= [-1];         % Lower Bound of Decision Variables
VarMax= [+1];         % Upper Bound of Decision Variables

VarMin=repmat(VarMin,1,nVar);
VarMax=repmat(VarMax,1,nVar);

% VarRange=[VarMin VarMax];

%% GA Parameter
for h = 1 : 1
MaxIt=3000;         % Maximum Number of Iterations

nPop=100;            % Population Size

pCrossover=0.9;     % Parents (Offsprings) Population Size Ratio
nCrossover=round(pCrossover*nPop/2)*2;

pMutation=0.2;      % Mutants Population Size Ratio
% nMutation=round(pMutation*nPop);
nMutation=round(pMutation*nPop);

nelitism = max(nPop-(nCrossover+nMutation),2); 
SelectionPressure=10;

%% Initialization

individual.Position=[];
individual.Cost=[];

pop=repmat(individual,nPop,1);

for i=1:nPop
    pop(i).Position=unifrnd(VarMin,VarMax,VarSize);

    pop(i).Cost=CostFunction(pop(i).Position,FCMStr);
end

% Sort Population
Costs=[pop.Cost];
[Costs SortOrder]=sort(Costs);
pop=pop(SortOrder);
WorstCost=Costs(end);

BestSol=[];
BestCost=zeros(MaxIt,1);
MeanCost=zeros(MaxIt,1);

nfe=zeros(MaxIt,1);

%% GA Main Loop

for it=1:MaxIt
    
    % Selection Probabilities
    Costs=[pop.Cost];

%     Scaled_Costs = (max(Costs)-Costs);          % crossover roulete
%     P=Scaled_Costs/sum(Scaled_Costs);           % crossover roulete

%     P=exp(-SelectionPressure*Costs/WorstCost);  % crossover roulete
%     P=P/sum(P);                                 % crossover roulete

    inds = MySelectionFcn(Costs,nCrossover,1);
    
    % Crossover
    pop2=repmat(individual,nCrossover/2,2);
    
    zz=1;
%     for k=1:nCrossover/2                   % crossover roulete
    for k=1:2:nCrossover
              
%         i1=RouletteWheelSelection(P);      % crossover roulete  
%         i2=RouletteWheelSelection(P);      % crossover roulete   
%         p1=pop(i1);                        % crossover roulete
%         p2=pop(i2);                        % crossover roulete

        p1=pop(inds(k));
        p2=pop(inds(k+1));

        [pop2(zz,1).Position pop2(zz,2).Position]=Crossover(p1.Position,p2.Position);
        
        pop2(zz,1).Position=min(max(pop2(zz,1).Position,VarMin),VarMax);
        pop2(zz,2).Position=min(max(pop2(zz,2).Position,VarMin),VarMax);

        
        pop2(zz,1).Cost=CostFunction(pop2(zz,1).Position,FCMStr);
        pop2(zz,2).Cost=CostFunction(pop2(zz,2).Position,FCMStr);
        zz=zz+1;
    end
    pop2=pop2(:);            
    
    % Mutation
    pop3=repmat(individual,nMutation,1);
    for k=1:nMutation
        
%         i=randi([1 nPop]);        
% %         pop3(k).Position=Mutate(pop(i).Position,VarRange);
%         pop3(k).Position=Mutate(pop(i).Position,VarMin,VarMax);
%         pop3(k).Position=min(max(pop3(k).Position,VarMin),VarMax);
        
        pop3(k).Position=unifrnd(VarMin,VarMax,VarSize);
        
        pop3(k).Cost=CostFunction(pop3(k).Position,FCMStr);
        
    end
    
    % Merge Populations
    pop=[pop(1:nelitism)
         pop2
         pop3];  %#ok
    
    % Sort Population
    Costs=[pop.Cost];
    [Costs SortOrder]=sort(Costs);
    pop=pop(SortOrder);
    WorstCost=max(WorstCost,Costs(end));
    
    % Delete Extra Individuals
    pop=pop(1:nPop);
    Costs=Costs(1:nPop);
    
    % Save Results
    BestSol=pop(1);
    BestCost(it)=Costs(1);
    MeanCost(it)=mean(Costs);
%     nfe(it)=NFE;
    
    % Show Information
    disp(['Iteration ' num2str(it) ':   ' ... 
        'Best Cost = ' num2str(BestCost(it)) '  , ' ...
          'Mean Cost = ' num2str(MeanCost(it))]);
end

%% Results

BestCost = BestCost(end);
BestSolution = BestSol.Position;
        
%% best values for FCM nodes
W = zeros(FCMStr.NodesNo,FCMStr.NodesNo);

%% density smaller than 100%
for ii = 1 : weightsNo
           W(Wrows(ii),Wcols(ii)) = BestSolution(ii);
end

NodesValue = zeros(size(data,1),FCMStr.NodesNo);
NodesValue(1,:) = data(1,:);
for jj = 2 : size(data,1)
    NodesValue(jj,:) = NodesValue(jj-1,:) * W;
    NodesValue(jj,:) = 1 ./ (1 + exp(- FCMStr.landa .* NodesValue(jj,:)));
%     if  NodesValue(jj,:)~=0    % added by sadra
%         NodesValue(jj,:) = 1 ./ (1 + exp(- FCMStr.landa .* NodesValue(jj,:)));
%     else
%         NodesValue(jj,:)=NodesValue(jj-1,:); % added by sadra
%     end
    
%     NodesValue(jj,1)=NodesValue(1,1);
%     NodesValue(jj,2)=NodesValue(1,2);
    
%     NodesValue(jj,8)=NodesValue(1,8);
%     NodesValue(jj,9)=NodesValue(1,9);
%     NodesValue(jj,10)=NodesValue(1,10);
%     NodesValue(jj,12)=NodesValue(1,12);
%     NodesValue(jj,13)=NodesValue(1,13);
%     NodesValue(jj,14)=NodesValue(1,14);
%     NodesValue(jj,15)=NodesValue(1,15);
%     NodesValue(jj,16)=NodesValue(1,16);
%     NodesValue(jj,17)=NodesValue(1,17);
%     NodesValue(jj,18)=NodesValue(1,18);
%     NodesValue(jj,19)=NodesValue(1,19);

end

figureStr = ['save(' '''figureData_' num2str(h) '.mat'',' '''BestCost'',' '''MeanCost'')'];
% eval(figureStr)
nodeStr = ['save(' '''NodesValue_' num2str(h) '.mat'',' '''NodesValue'')'];
% eval(nodeStr)
weightStr = ['save(' '''Weights_Results_' num2str(h) '.mat'',' '''W'')'];
% eval(weightStr)

errors = data - NodesValue;
elementsNo = (size(NodesValue,1)-1) * size(NodesValue,2);
bestCost(h) = sum(sum(abs(errors)))/elementsNo
bestCost_2(h) = sum(sum((abs(errors).^2)))/elementsNo
end

bestCost
bestCost_2
avg_bestCost = mean(bestCost)
avg_bestCost_2 = mean(bestCost_2)
s_bestCost = std(bestCost)
s_bestCost_2 = std(bestCost_2)


% save('FCMResults.mat','bestCost','bestCost_2','avg_bestCost','avg_bestCost_2','s_bestCost','s_bestCost_2')


% end