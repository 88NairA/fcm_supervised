function x=Mutate(x,VarMin,VarMax)

% %     VarMin=min(VarRange);
% %     VarMax=max(VarRange);

    nVar=numel(x);
    
    j=randi([1 nVar]);
    z=randn;
    r=tanh(0.1*z);
    
    if r<0
        x(j)=x(j)+(x(j)-VarMin(j))*r;
    else
        x(j)=x(j)+(VarMax(j)-x(j))*r;
    end
    
    
%         VarSize=[1 nVar];    
%         x=unifrnd(VarMin,VarMax,VarSize);

end