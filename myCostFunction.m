function cost = myCostFunction(pop)

global V dV dX a

% cost = zeros(size(pop,1),1);
c = pop(1);
m = pop(2);
l = pop(3);
a_hat = c .* V.^m .* ( dV./dX.^l );
cost = sqrt( sum( (a-a_hat).^2 ) / size(a,1) );
% cost = sqrt( sum( (a-a_hat).^2 ) / size(a,1) )/ (sqrt(sum( a.^2 ) / size(a,1))+sqrt(sum( a_hat.^2 ) / size(a,1)));  % result in large mes



