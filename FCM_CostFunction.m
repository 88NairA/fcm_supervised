function cost = FCM_CostFunction(POP,FCMStr)

global Wrows Wcols weightsNo;

%%
load FCM_mainData_7Nodes.mat
data = FCM_mainData;
%%
% load FCM20_60.mat 
% data = FCM20_60;

landa = FCMStr.landa;
maxIt = size(data,1);
NodesNo = FCMStr.NodesNo;

% for nn = 1 : size(POP,1)
    indv = [];
    indv = POP;
    errors = [];
    totalError = [];

W = zeros(NodesNo,NodesNo);
% %% density = 100%
% for ii = 1 : NodesNo
%     W(ii,:) = POP(((ii*NodesNo)-(FCMStr.NodesNo - 1)) : NodesNo*ii);
% end
%% density smaller than 100%
for ii = 1 : weightsNo
    W(Wrows(ii),Wcols(ii)) = indv(ii);
end

%%
NodesValue = zeros(maxIt,NodesNo);
NodesValue(1,:) = data(1,:);
for jj = 2 : maxIt
    NodesValue(jj,:) = NodesValue(jj-1,:) * W;
    NodesValue(jj,:) = 1 ./ (1 + exp(- landa .* NodesValue(jj,:)));
%     if NodesValue(jj,:)~=0   % added by sadra
%         NodesValue(jj,:) = 1 ./ (1 + exp(- landa .* NodesValue(jj,:)));
%     else
%         NodesValue(jj,:)=NodesValue(jj-1,:);   % added by sadra
%     end
    
%     NodesValue(jj,1)=NodesValue(1,1);
%     NodesValue(jj,2)=NodesValue(1,2);

%     NodesValue(jj,8)=NodesValue(1,8);
%     NodesValue(jj,9)=NodesValue(1,9);
%     NodesValue(jj,10)=NodesValue(1,10);
%     NodesValue(jj,12)=NodesValue(1,12);
%     NodesValue(jj,13)=NodesValue(1,13);
%     NodesValue(jj,14)=NodesValue(1,14);
%     NodesValue(jj,15)=NodesValue(1,15);
%     NodesValue(jj,16)=NodesValue(1,16);
%     NodesValue(jj,17)=NodesValue(1,17);
%     NodesValue(jj,18)=NodesValue(1,18);
%     NodesValue(jj,19)=NodesValue(1,19);
        
    %     for kk = 1 : NodesNo
    %         tempW = [];
    %         tempW = W(:,kk);
    %         tempW = tempW';
    %         NodesValue(jj,kk) = sum(NodesValue(jj-1,:) .* tempW(1,:));
    %         NodesValue(jj,kk) = 1 / (1 + exp(- 0.1*NodesValue(jj,kk)));
    %     end
end
    
    errors = data - NodesValue;
    % totalError = mse(errors);
    elementsNo = (size(NodesValue,1)-1) * size(NodesValue,2);
    totalError = sum(sum((abs(errors).^2)))/ elementsNo;
    cost = totalError;
    
% end

