function cost = gippsCostFunction(pop)

global T Vntt dX_L Vn Vn_1

Bn = pop(1);
D = pop(2);
Bhasht = pop(3);
Vntt_p = (Bn .* T) + sqrt((((Bn^2) .* (T^2)) - (Bn .*((2 .*(dX_L - D)) - (Vn .* T) - ((Vn_1 .^ 2) ./ Bhasht ))) ));
% pnt = abs(Bn*T) -(((Bn^2) .* (T^2)) - (Bn .*((2 .*(dX_L - D)) - (Vn .* T) - ((Vn_1 .^ 2) ./ Bhasht ))) );
% pnt = (((Bn^2) .* (T^2)) - (Bn .*((2 .*(dX_L - D)) - (Vn .* T) - ((Vn_1 .^ 2) ./ Bhasht ))) );
% pnt = ((Bn*T).^2) -(((Bn^2) .* (T^2)) - (Bn .*((2 .*(dX_L - D)) - (Vn .* T) - ((Vn_1 .^ 2) ./ Bhasht ))) );
% pnt2 = -(((Bn^2) .* (T^2)) - (Bn .*((2 .*(dX_L - D)) - (Vn .* T) - ((Vn_1 .^ 2) ./ Bhasht ))) );
% penalty = (pnt+ abs(pnt))./2;
% penalty2 = (pnt2+ abs(pnt2))./2;

pnt3 = -Vntt_p;
penalty3 = (pnt3+ abs(pnt3))./2;
% cost = sqrt(mse((Vntt-Vntt_p)))+sum(penalty)+ sum(penalty2) ;
cost = sqrt(mse((Vntt-Vntt_p)))+sum(penalty3)*2 ;
% cost = sqrt( sum( (Vntt-Vntt_p).^2 ) / size(Vntt,1) );
% cost = sqrt( sum( (a-a_hat).^2 ) / size(a,1) )/ (sqrt(sum( a.^2 ) / size(a,1))+sqrt(sum( a_hat.^2 ) / size(a,1)));  % result in large mes
% cost = sum( (Vntt-Vntt_p).^2 ) /  sum((Vntt-mean(Vntt)).^2);


